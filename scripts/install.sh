#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"

for conf in /usr/local/directadmin/data/users/*/user.conf; do
    if [ 0`grep -c skin=power_ru $conf` -gt 0 ]; then
	perl -pi -e 's#language=en#language=ru#' $conf
    fi
done

echo "Plugin Installed!"; #NOT! :)

chmod 755 $DOCUMENT_ROOT/../admin

exit 0;
