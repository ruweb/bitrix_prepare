#!/bin/sh
exec 2>&1
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
echo "<PRE>"
set -e

set -x

set +x
echo "Plugin has been updated!"; #NOT! :)

exit 0;
